Package.describe({
  name: 'synthfx:meteor-cs-ldap',
  version: '1.0.3',
  summary: 'Meteor accounts login using ldapjs. Customized for CS POI.',
  git: '',
  documentation: 'README.md'
});


Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');
  api.use(['synthfx:cs-ldapjs@1.0.0'], 'server');

  api.use('accounts-base', 'server');
  api.imply('accounts-base', ['client', 'server']);
  api.imply('accounts-password', ['client', 'server']);

  api.use('check');

  api.addFiles(['ldap_client.js'], 'client');
  api.addFiles(['ldap_server.js'], 'server');

  api.export('LDAP', 'server');
  api.export('LDAP_DEFAULTS', 'server');
});
